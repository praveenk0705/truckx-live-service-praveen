package com.example.plugin;

import org.apache.cordova.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.apache.cordova.LOG;
import android.util.Log;

public class Hello extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray data, CallbackContext callbackContext) throws JSONException {
        String TAG = "praveenplugin";

        if (action.equals("greet")) {
           // webView.loadUrl("javascript:console.log('hello');");
            LOG.d(TAG, "We are entering execute");
            Log.i(TAG, "Your message to debug");
            System.out.println("praveenplugin");

            String name = data.getString(0);
            String message = "Hi there, " + name;
            callbackContext.success(message);

            return true;

        } else {
            
            return false;

        }
    }
}
