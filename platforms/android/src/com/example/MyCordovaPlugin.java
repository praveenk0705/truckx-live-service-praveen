/**
 */
package com.example;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.apache.cordova.PluginResult.Status;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;
import android.util.Log;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import java.net.*;

import android.util.Log;

import java.util.Date;

public class MyCordovaPlugin extends CordovaPlugin {
  
  private static final String TAG = "MyCordovaPlugin";
  private Socket mSocket;
  
    {
        try {
            mSocket = IO.socket("http://live.truckx.com");
        } catch (URISyntaxException e) {}
    }



  public void initialize(CordovaInterface cordova, CordovaWebView webView) {
    super.initialize(cordova, webView);

    Log.i(TAG, "Initializing MyCordovaPlugin");
  }

  public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {
    Log.i(TAG, "inside execute");
    if(action.equals("echo")) {
  //     String phrase = args.getString(0);
  //     // Echo back the first argument
  //     Log.d(TAG, phrase);
  //     Log.i(TAG, "in action echo");
  //     for(int i = 0 ; i < 100 ; i++){
  //       Log.i(TAG , "emitting ");
  //       try{
  //       Thread.sleep(1000);
  //           }
  //           catch(InterruptedException ex){
  // //do stuff
  //           }
  //     }

  mSocket.connect();


    } 
    
    else if(action.equals("getDate")) {
      // An example of returning data back to the web layer
      final PluginResult result = new PluginResult(PluginResult.Status.OK, (new Date()).toString());
      Log.i(TAG, "inside getdate action");
      callbackContext.sendPluginResult(result);
    }
    return true;
  }

}
